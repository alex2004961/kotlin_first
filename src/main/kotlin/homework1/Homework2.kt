package homework1

fun main() {
    val viski: Byte = 50
    val pinocolada: Short = 200
    val fresh: Long = 3000000000
    val el: Double = 0.666666667
    val lemonade: Int = 18500
    val cola: Float = 0.5F
    val something: String = "Что-то авторское!"

    println("Заказ - ‘$lemonade мл лимонада’ готов!")
    println("Заказ - ‘$pinocolada мл пина колады’ готов!")
    println("Заказ - ‘$viski мл виски’ готов!")
    println("Заказ - ‘$fresh капель фреша’ готов!")
    println("Заказ - ‘$cola литра колы’ готов!")
    println("Заказ - ‘$el литра эля’ готов!")
    println("Заказ - ‘$something’ готов!")
}
