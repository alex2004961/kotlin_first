package homeworkFinal

fun main() {
    val player1 = Player()
    val player2 = Player()
    player1.printYourName() // Игрок 1 вводит свое имя
    startTheGame(player1) // Игрок 1 вводит координаты своих кораблей
    player2.printYourName()
    startTheGame(player2)
    while(true){
        if(playerStep(player1, player2) == 0){
            player1.printWinnerName()
            break
        }   //player1 атакует player2
        if (playerStep(player2, player1) == 0){
            player2.printWinnerName()
            break
        }  //player2 атакует player1
    }
}
/*
Что можно улучшить:
обработать случай когда стреляешь по той же точке - РЕШЕНО!
не дать вписать неправильные значения - РЕШЕНО!
*/

/*
Правила заполнения поля боя:
*/
//0 - пусто
//1 - однопалубный корабль
//2 - палуба двупалубного корабля
//3 - подбит
//4 - взорван однопалубный
//5 - мимо
/*
    Функция для начала игры: заполнение полей
*/
fun startTheGame(player: Player){
    println("- ${player.getYourName()} заполняет поле")
    player.fillTheField()
    //раскоментить для дебага!
//    player.printTheField()
}
/*
    Функция для логики ходов игроков
 */
fun playerStep(player1: Player,player2: Player): Int{
    println("- Стреляет игрок ${player1.getYourName()}")
    var resultOfShoot1 = player2.shootTheShip()
    // раскоментить для дебага
//    player2.printTheField()
//    println(player2.countingOfShipPixels())
    if(player2.countingOfShipPixels() == 0) return 0
    while (resultOfShoot1<1){ //проверяем на возможность второго хода
        println("- Стреляет игрок ${player1.getYourName()}")
        resultOfShoot1 = player2.shootTheShip()
        // раскоментить для дебага
//        player2.printTheField()
//        println(player2.countingOfShipPixels())
        if(player2.countingOfShipPixels() == 0) return 0
    }
    return 1
}

class Player(private val field: MutableList<MutableList<Int>> = mutableListOf(mutableListOf(0,0,0,0), mutableListOf(0,0,0,0),mutableListOf(0,0,0,0),mutableListOf(0,0,0,0))){
    private var name: String = ""
    /*
    Функция для написания имени игрока
     */
    internal fun printYourName(){
        var isValidInput = false
        var userChoice: String
        while(!isValidInput) {
            println("Напишите свое имя:")
            name = readLine().toString()
            if(name != ""){
                isValidInput = true
                userChoice = name
            }
            if (!isValidInput) println("- Введено пустое значение")
        }
    }
    internal fun getYourName(): String{
        return name
    }
    /*
    Функция для вывода поля боя для дебага
     */
    internal fun printTheField(){
        field.forEach{el ->
            println(el)
        }
    }
    /*
    Функция для вывода имени победителя
     */
    internal fun printWinnerName(){
        println("Победитель $name! Поздравляем!")
    }
    /*
    Функция для заполнения поля боя перед боем
     */
    internal fun fillTheField(){
        enterOnceShip("первого")
        enterOnceShip("второго")
        enterDoubleShip()
    }
    /*
    Функция подсчета уцелевших кораблей
    */
    internal fun countingOfShipPixels(): Int{
        var counterOfShipPixels = 0
        field.forEach{el ->
            el.forEach{ item ->
                when (item) {1,2 -> counterOfShipPixels ++}
            }
        }
        return counterOfShipPixels
    }
    /*
    Функция для ввода координат однопалубного корабля
     */
    private fun enterOnceShip(countOfOnceShip: String){
        println("- Вводим координаты для $countOfOnceShip однопалубного корабля (два числа от 0 до 3 через пробел)")
        val x: Int = number("Первое")
        val y: Int = number("Второе")
        field[x][y] = 1
    }
    /*
    Функция для ввода координаты и ее проверки
     */
    fun number(coodrdinate: String): Int {
        var isValidInput = false
        var userChoice = 0
        while(!isValidInput) {
            println("Введите $coodrdinate число координаты")
            val userInput = readLine()?.toIntOrNull()
            if (userInput != null) {
                if ((userInput >= 0) and ((userInput) < 4)) {
                    isValidInput = true
                    userChoice = userInput
                }
            }
            if (!isValidInput) println("- Введено неверное значение, ожидается число от 0 до 3, попробуйте снова.")
        }
        return userChoice
    }
    /*
    Функция для ввода координат двупалубного корабля
     */
    private fun enterDoubleShip(){
        println("- Введите первые координаты для двупалубного корабля (два числа от 0 до 3 через пробел)")
        val x1= number("первое")
        val y1 = number("второе")
        field[x1][y1] = 2
        println("- Введите вторые координаты для двупалубного корабля (два числа от 0 до 3 через пробел)")
        val x2= number("первое")
        val y2 = number("второе")
        field[x2][y2] = 2
    }
    /*
    Функция для выстрела
     */
    internal fun shootTheShip(): Int{
        val x= number("первое")
        val y = number("второе")
        when(field[x][y]){
            0 -> {println("Мимо!")
                field[x][y]=5
                return 1} //единственный случай, когда нам надо закончить ход игрока
            1 -> {println("Однопалубный корабль взорван, введите новые координаты")
                field[x][y]=4
                return 0}
            2 -> {checkingDoubleShip(x,y)
                return 0}
            3 -> {println("Вы уже стреляли по этой точке, введите новые координаты")
                field[x][y]=3
                return 0
            }
            4 -> {println("Вы уже стреляли по этой точке, введите новые координаты")
                field[x][y]=4
                return 0
            }
            5 -> {println("Вы уже стреляли по этой точке, введите новые координаты")
                field[x][y]=5
                return 0
            }
        }
        return 100
    }
    /*
    Функция для проверки после выстрела по двупалубному кораблю
     */
    private fun checkingDoubleShip(x: Int, y: Int){
        if((x > 0) and (x < 3) and (y > 0) and (y < 3)){ // нахожу 4 центральные точки на поле
            if ((field[x-1][y] == 2) or (field[x+1][y] == 2) or (field[x][y-1] == 2) or (field[x][y+1] == 2)){// проверяю эти точки на наличие второй палубы рядом
                holeInTheShip(x,y) //сообщаю, что еще не весь корабль взорван
            } else {
                shipBoom(x,y) //сообщаю, что весь корабль удалось подбить
            }
        }
        //далее повторяю все тоже самое с другими точками. При этом первым if-ом стараюсь не выходить за рамки массива
        if((x == 0) and (y==0)){ //нахожу угловую точку
            if ((field[x+1][y] == 2) or (field[x][y+1] == 2)){
                holeInTheShip(x,y)
            } else {
                shipBoom(x,y)
            }
        }
        if((x > 0) and (x < 3) and (y==0)){ // нахожу две боковых точки
            if ((field[x-1][y] == 2) or (field[x+1][y] == 2) or (field[x][y+1] == 2)){
                holeInTheShip(x,y)
            } else {
                shipBoom(x,y)
            }
        }
        if((x == 3) and (y == 0)){
            if ((field[x-1][y] == 2) or (field[x][y+1] == 2)){
                holeInTheShip(x,y)
            } else {
                shipBoom(x,y)
            }
        }
        if((x == 3) and (y < 3) and (y > 0)){
            if ((field[x-1][y] == 2) or (field[x][y-1] == 2) or (field[x][y+1] == 2)){
                holeInTheShip(x,y)
            } else {
                shipBoom(x,y)
            }
        }
        if((x == 3) and (y == 3)){
            if ((field[x][y-1] == 2) or (field[x-1][y] == 2)){
                holeInTheShip(x,y)
            } else {
                shipBoom(x,y)
            }
        }
        if((x > 0) and (x < 3) and (y == 3)){
            if ((field[x-1][y] == 2) or (field[x+1][y] == 2) or (field[x][y-1] == 2)){
                holeInTheShip(x,y)
            } else {
                shipBoom(x,y)
            }
        }
        if((x == 0) and (y == 3)){
            if ((field[x+1][y] == 2) or (field[x][y-1] == 2)){
                holeInTheShip(x,y)
            } else {
                shipBoom(x,y)
            }
        }
        if((x == 0) and (y > 0) and (y < 3)){
            if ((field[x+1][y] == 2) or (field[x][y-1] == 2) or (field[x][y+1] == 2)){
                holeInTheShip(x,y)
            } else {
                shipBoom(x,y)
            }
        }
    }
    /*
    Функция для ранения двупалубного корабля
     */
    private fun holeInTheShip(x: Int,y: Int){
        println("Двупалубный корабль ранен")
        field[x][y]=3
    }
    /*
    Функция для взрыва двупалубного корабля
     */
    private fun shipBoom(x: Int,y: Int){
        println("Двупалубный корабль взорван")
        field[x][y]=3
    }
}
