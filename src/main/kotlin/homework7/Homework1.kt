package homework7

fun main() {
    entryLoginAndPassword("32dfsgdfagfdfsds","fdsffdsfsafd","fdsffdsfsafd")
}

//write your function and Exception-classes here

class User(val login: String, val password: String)

fun entryLoginAndPassword(login: String, password: String, passwordConfirmation: String): User{
    if(login.length !in 1..20)
        throw WrongLoginException("в логине не должно быть более 20 символов")
    if(password.length < 10){
        throw WrongPasswordException("в пароле не должно быть менее 10 символов")
    }
    if(password != passwordConfirmation){
        throw WrongPasswordException("пароль и подтверждение пароля должны совпадать")
    }
    return User(login,password)
}

class WrongLoginException(message: String) : Exception(message)

class WrongPasswordException(message: String) : Exception(message)
