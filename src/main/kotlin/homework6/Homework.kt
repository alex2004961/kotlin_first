package homework6

fun main(){
    val lvenok = Lion("Koko", 3,4)
    lvenok.eat("шашлык")
    val tigrenok = Tiger("Lulu", 3,4)
    tigrenok.eat("яйцо")
    val hipotik = Hippopotamus("Hurok",5,10)
    hipotik.eat("мясо")
    val volchok = Wolf("Auff",3,3)
    volchok.eat("шаверма")
    val girik = Giraffe("Vi",15,8)
    girik.eat("листья")
    val slonik = Elephant("Vu", 7,12)
    slonik.eat("арбуз")
    val makaka = Chimpanzee("UaUa",2,2)
    makaka.eat("бананы")
    val gorilka = Gorilla("Bubu",4,5)
    gorilka.eat("мясо")
    groupEat(arrayOf(Tiger("Lulu", 3,4),Giraffe("Vi",15,8),
        Hippopotamus("UaUa",2,2),Wolf("Bubu",4,5)), arrayOf("мясо", "бананы", "арбуз", "листья", "шаверма")
    )
}
abstract class Animal {
    abstract val name: String
    abstract val height: Int
    abstract val weight: Int
    abstract val foodPreferences: Array<String>
    private var satiety = 0
    fun eat(food: String){
        for (item in foodPreferences){
            if (item == food) satiety++
        }
    }
}
class Lion(override val name: String, override val height: Int, override val weight: Int) : Animal(){
    override val foodPreferences = arrayOf("мясо","шашлык")
}
class Tiger(override val name: String, override val height: Int, override val weight: Int) : Animal(){
    override val foodPreferences = arrayOf("мясо","шашлык")
}
class Hippopotamus(override val name: String, override val height: Int, override val weight: Int) : Animal(){
    override val foodPreferences = arrayOf("капуста","трава","арбуз")
}
class Wolf(override val name: String, override val height: Int, override val weight: Int) : Animal(){
    override val foodPreferences = arrayOf("мясо","шаверма")
}
class Giraffe(override val name: String, override val height: Int, override val weight: Int) : Animal(){
    override val foodPreferences = arrayOf("листья","бананы","яблоки")
}
class Elephant(override val name: String, override val height: Int, override val weight: Int) : Animal(){
    override val foodPreferences = arrayOf("капуста","арбуз")
}
class Chimpanzee(override val name: String, override val height: Int, override val weight: Int) : Animal(){
    override val foodPreferences = arrayOf("бананы","яблоки","арбуз")
}
class Gorilla(override val name: String, override val height: Int, override val weight: Int) : Animal(){
    override val foodPreferences = arrayOf("мясо","бананы")
}

fun groupEat(animals: Array<Animal>, foodPreferences: Array<String>){
    for(animal in animals){
        for(food in foodPreferences){
            animal.eat(food)
        }
    }
}
