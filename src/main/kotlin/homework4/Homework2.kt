package homework4

import kotlin.math.roundToInt

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var five = 0
    var four = 0
    var three = 0
    var two = 0
    for (item in marks){
        when (item) {
            5 -> five++
            4 -> four++
            3 -> three++
            2 -> two++
        }
    }
    println("Отличников - ${(10*100.0*five/marks.size).roundToInt()/10.0}%")
    println("Хорошистов - ${(10*100.0*four/marks.size).roundToInt()/10.0}%")
    println("Троечников - ${(10*100.0*three/marks.size).roundToInt()/10.0}%")
    println("Двоечников - ${(10*100.0*two/marks.size).roundToInt()/10.0}%")
}
