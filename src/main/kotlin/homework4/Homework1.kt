package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var countForJoe = 0
    var countForTeam = 0
    for (item in myArray) {
        if (item % 2 == 0) {
            countForJoe++
        } else countForTeam++
    }
    println("Джо получит $countForJoe четных монет")
    println("Команда получит $countForTeam нечетных монет")
}
