package homework5

fun main() {
    val digit = readLine()!!.toInt()
    //call written function here
    println(perevorot(digit))
}

fun perevorot(digit: Int): Int {
    var newNumber = ""
    var i = digit
    while (i > 0) {
        newNumber += "${(i % 10)}"
        i /= 10
    }
    return newNumber.toInt()
}