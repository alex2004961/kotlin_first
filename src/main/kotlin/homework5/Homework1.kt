package homework5

fun main(){
    val lvenok = Lion("Koko", 3,4)
    println(lvenok.satiety)
    lvenok.eat("шашлык")
    println(lvenok.satiety)
    val tigrenok = Tiger("Lulu", 3,4)
    tigrenok.eat("яйцо")
    val hipotik = Hippopotamus("Hurok",5,10)
    hipotik.eat("мясо")
    val volchok = Wolf("Auff",3,3)
    volchok.eat("шаверма")
    val girik = Giraffe("Vi",15,8)
    girik.eat("листья")
    val slonik = Elephant("Vu", 7,12)
    slonik.eat("арбуз")
    val makaka = Chimpanzee("UaUa",2,2)
    makaka.eat("бананы")
    val gorilka = Gorilla("Bubu",4,5)
    gorilka.eat("мясо")
}
class Lion(val name: String, val height: Int, weight: Int){
    val foodPreferences = arrayOf("мясо","шашлык")
    var satiety = 0
    fun eat(food: String){
        for (item in foodPreferences){
            if (item == food) satiety++
        }
    }
}
class Tiger(val name: String, val height: Int, weight: Int){
    val foodPreferences = arrayOf("мясо","шашлык")
    var satiety = 0
    fun eat(food: String){
        for (item in foodPreferences){
            if (item == food) satiety++
        }
    }
}
class Hippopotamus(val name: String, val height: Int, weight: Int){
    val foodPreferences = arrayOf("капуста","трава","арбуз")
    var satiety = 0
    fun eat(food: String){
        for (item in foodPreferences){
            if (item == food) satiety++
        }
    }
}
class Wolf(val name: String, val height: Int, weight: Int){
    val foodPreferences = arrayOf("мясо","шаверма")
    var satiety = 0
    fun eat(food: String){
        for (item in foodPreferences){
            if (item == food) satiety++
        }
    }
}
class Giraffe(val name: String, val height: Int, weight: Int){
    val foodPreferences = arrayOf("листья","бананы","яблоки")
    var satiety = 0
    fun eat(food: String){
        for (item in foodPreferences){
            if (item == food) satiety++
        }
    }
}
class Elephant(val name: String, val height: Int, weight: Int){
    val foodPreferences = arrayOf("капуста","арбуз")
    var satiety = 0
    fun eat(food: String){
        for (item in foodPreferences){
            if (item == food) satiety++
        }
    }
}
class Chimpanzee(val name: String, val height: Int, weight: Int){
    val foodPreferences = arrayOf("бананы","яблоки","арбуз")
    var satiety = 0
    fun eat(food: String){
        for (item in foodPreferences){
            if (item == food) satiety++
        }
    }
}
class Gorilla(val name: String, val height: Int, weight: Int){
    val foodPreferences = arrayOf("мясо","бананы")
    var satiety = 0
    fun eat(food: String){
        for (item in foodPreferences){
            if (item == food) satiety++
        }
    }
}